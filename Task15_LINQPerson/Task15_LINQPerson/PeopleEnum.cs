﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Task16_LINQPerson
{
    class PeopleEnum :IEnumerator
    {
        public Person[] people;
        int position = -1;

        public PeopleEnum(Person[] list)
        {
            people = list;
        }

        public bool MoveNext()
        {
            position++;
            return (position < people.Length);
        }

        public void Reset()
        {
            position = -1;
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        public Person Current
        {
            get
            {
                try
                {
                    return people[position];
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }
            }
        }
    }
}
