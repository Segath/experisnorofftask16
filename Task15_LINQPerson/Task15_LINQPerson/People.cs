﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Task16_LINQPerson
{
    class People : IEnumerable
    {
        private Person[] people;
        public People(Person[] p)
        {
            people = new Person[p.Length];

            for (int i = 0; i < p.Length; i++)
            {
                people[i] = p[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }

        public PeopleEnum GetEnumerator()
        {
            return new PeopleEnum(people);
        }
    }
}
