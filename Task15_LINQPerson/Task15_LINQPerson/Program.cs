﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task16_LINQPerson
{
    class Program
    {
        static void Main(string[] args)
        {
            Person[] arrayOfPersons = new Person[]
            {
                new Person("Jon Erik", "Selland", "23452143", "Room"),
                new Person("Sondre", "Waage Tofte", "456243573", "Home"),
                new Person("Anders", "Tranberg", "+23457023452", "Denmark"),
                new Person("Bernt", "Odland", "234502340", "Bergen"),
                new Person("Solveig", "Kopperdal", "42092345", "Somewhere")
            };

            People customCollPeople = new People(arrayOfPersons);
            Search(customCollPeople, arrayOfPersons);
        }

        public static void Search(People listOfPersons, Person[] arrayOfPeople)
        {
            do
            {
                Console.WriteLine("Type /quit to quit");
                Console.Write("Search for name: ");
                string searchString = Console.ReadLine();

                #region check input from user and search
                if (searchString == "")
                {
                    foreach (Person person in listOfPersons)
                    {
                        Console.WriteLine(person.ToString());
                    }
                }
                else if (searchString == "/quit")
                {
                    break;
                }
                else
                {
                    IEnumerable<Person> searchQuery =
                        from p in arrayOfPeople
                        where p.ContainsName(searchString)
                        select p;

                    foreach(Person person in searchQuery)
                    {
                        Console.WriteLine(person.ToString());
                    }
                }
                #endregion
                Console.WriteLine();
            } while (true);
        }
    }
}
