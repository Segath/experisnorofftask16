﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task16_LINQPerson
{
    class Person
    {

        public Person() { }
        public Person(string firstName, string lastName, string telephone, string address)
        {
            FirstName = firstName;
            LastName = lastName;
            Telephone = telephone;
            Address = address;
        }

        private string firstName;
        private string lastName;
        private string telephone;
        private string address;

        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public string Telephone { get => telephone; set => telephone = value; }
        public string Address { get => address; set => address = value; }

        public override string ToString()
        {
            return $"{FirstName} {LastName} has the phone number of {Telephone} and lives in {Address}";
        }

        public bool ContainsName(string searchString)
        {
            if(FirstName.Contains(searchString, StringComparison.OrdinalIgnoreCase) ||
               LastName.Contains(searchString, StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }
            return false;
        }
    }
}
